# README

This is the application you are required to fix and maintain it uses ruby version 2.3.1 and rails version 5.1

There is nothing special technically. To clone the app just 
```bash
git clone https://instilleducation@bitbucket.org/instilleducation/instill.git
```

To run the application locally
```bash
bundle install
rails s
```

To push changes to the git  bitbucket repository
```bash
git push
```

To push changes to heroku
```bash
git push heroku master
```



## Database creation

Heroku forces you to use a postgresql database in production but I am using a sqlite database in development because it is the default.
```bash
rails db:create
rails db:setup
rails db:migrate
```


No Testing 
I am sorry!
Really, Really Sorry

##Google Data Studio
We use Google Data Studio to share data with our clients.

So we connect the postgresql database to this and manage the data in Google Data Studio using simple SQL.




##Generating Keys and certificates for Google Data Studio 
The keys and certificates are in the Google Drive but if something goes wrong you can generate them yourself.

To get the client key and certificate use openssl
```bash
openssl genrsa -des3 -out ca.key 4096
openssl req -new -x509 -days 365 -key ca.key -out ca.crt

```
This will create ca.key and ca.crt in your current directory.
To get the server certificate use the following python 2 code
https://raw.githubusercontent.com/thusoy/postgres-mitm/master/postgres_get_server_cert.py
when running the code the argument is the address of the database.
This will create a server.crt/pem file in your current directory.