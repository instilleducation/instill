class InPerson < ApplicationRecord
	has_one :programme_session,:as => :profile,dependent: :destroy
	has_many :attendances, dependent: :destroy
	belongs_to :exit_ticket, optional: true
	has_many :questions, through: :exit_ticket
	has_many :exit_ticket_answers, dependent: :destroy

	scope :programme, -> (programme) {joins(programme_session: :programme).where(programme_sessions: {programme: programme})}
  scope :school, ->(school) {joins(programme_session: :school).where(programme_sessions: {school: school})}
 
  def self.export_to_spreadsheet
    authorizer = Google::Auth::ServiceAccountCredentials.make_creds(json_key_io: File.open('config/Dashboard Data.json'),scope: Google::Apis::SheetsV4::AUTH_SPREADSHEETS)
    authorizer.fetch_access_token!
    @sheet_service = Google::Apis::SheetsV4::SheetsService.new
    @sheet_service.authorization = authorizer
    request_body = Google::Apis::SheetsV4::ValueRange.new
    spreadsheet_id = "1AbhW_IIkddlFXvv8d8xM3Q2l31DMzSTKI3UkjiDgP_Y"
    sheet_name = "in_people"
    request_body.major_dimension = "ROWS"
    
    request_body.values = get_information
    request_body.range = "#{sheet_name}!A1:Z#{get_information.length}"
    sheet_response = @sheet_service.update_spreadsheet_value(spreadsheet_id,request_body.range,request_body,value_input_option:"USER_ENTERED")
  end

  def self.get_information
    array = [["id","Name","Date","Scheduled Date", "Type","School Name"]]
    all.each do |session|
      array.push([session.id, session.name, session.date_done, session.scheduled_date, session.in_person_type, session.programme_session.school.name])    
    end
    array
  end 
      

end
