class Programme < ApplicationRecord
	has_many :developments, dependent: :destroy
	has_many :schools, through: :developments
	has_many :programme_sessions, dependent: :destroy
	has_many :sections, dependent: :destroy
	has_many :categories, through: :sections
	has_many :criteria, through: :categories
	has_many :conversations,  dependent: :destroy
	has_many :observations, through: :programme_sessions, as: :profile, dependent: :destroy, source: :profile, source_type: 'Observation'
	has_many :in_people, through: :programme_sessions, as: :profile, dependent: :destroy, source: :profile, source_type: 'InPerson'
	has_many :self_work, through: :programme_sessions, as: :profile, dependent: :destroy, source: :profile, source_type: 'SelfWork'
	has_many :scores, through: :criteria
end
