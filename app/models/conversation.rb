class Conversation < ApplicationRecord
  belongs_to :programme
  belongs_to :school

  scope :programme, -> (programme) {where(programme: programme)}



  def self.export_to_spreadsheet
    authorizer = Google::Auth::ServiceAccountCredentials.make_creds(json_key_io: File.open('config/Dashboard Data.json'),scope: Google::Apis::SheetsV4::AUTH_SPREADSHEETS)
    authorizer.fetch_access_token!
    @sheet_service = Google::Apis::SheetsV4::SheetsService.new
    @sheet_service.authorization = authorizer
    request_body = Google::Apis::SheetsV4::ValueRange.new
    spreadsheet_id = "1AbhW_IIkddlFXvv8d8xM3Q2l31DMzSTKI3UkjiDgP_Y"
    sheet_name = "conversations"
    request_body.major_dimension = "ROWS"
    
    request_body.values = get_information
    request_body.range = "#{sheet_name}!A1:Z#{get_information.length}"
    sheet_response = @sheet_service.update_spreadsheet_value(spreadsheet_id,request_body.range,request_body,value_input_option:"USER_ENTERED")
  end

  def self.get_information
    array = [["id","School Name","Month","Status","Date"]]
    all.each do |conversation|
      array.push([conversation.id, conversation.school.name, Date::MONTHNAMES[conversation.month.month], conversation.status, conversation.month])
    end
    array
  end

end 
