require 'google/apis/sheets_v4'
require 'googleauth'
class Teacher < ApplicationRecord
  belongs_to :school
  validates :school, presence:true
  validates :full_name, presence: true
  has_many :submissions
  has_many :feedbacks
  has_many :attendances
  has_many :scores, through: :feedbacks
  scope :school, ->(school) {where(school_id: school)}
  scope :teacher_phase, ->(teacher_phase) {where(grade: teacher_phase)}
  scope :observer, ->(observer) {joins(feedbacks: :user).where(feedbacks: {user: observer})}
  scope :observation_order, -> (observation_order) {joins(feedbacks: :observation).where(feedbacks:{observations: {observation_order: observation_order}})}
  scope :current_teachers, ->{where.not(role:"Left School")}
  scope :prefer_email, ->() {where(["not role = ? and communication like ?", "Left School", '%Email%'])}
  scope :prefer_sms, ->() {where(["not role = ? and communication like ?", "Left School", '%SMS%'])}

  	def self.to_csv(options = {})
      attributes = Teacher.column_names
      CSV.generate(headers: true) do |csv|
      csv << attributes
        all.each do |user|
          csv << attributes.map {|attr| user.send(attr)}
        end
      end
   	end

    def self.to_contacts_csv(options = {})
      attributes = ["teacher","school","Phone Number","email","status"]
      CSV.generate(headers: true) do |csv|
      csv << attributes
      all.each do |teacher|
        row = [teacher.full_name,teacher.school.name,
          teacher.phone ,teacher.email ,teacher.status]
        csv << row
        end
      end
    end


  def self.import(file)
	  spreadsheet = Roo::Spreadsheet.open(file.path)
	  header = spreadsheet.row(1)
	  (2..spreadsheet.last_row).each do |i|
	    row = Hash[[header, spreadsheet.row(i)].transpose]
	    product = find_or_create_by(full_name: row["full_name"]) 
	    product.attributes = row.to_hash
	    product.save!
  	end
	end

	def self.open_spreadsheet(file)
	  case File.extname(file.original_filename)
	  when ".csv" then Roo::CSV.new(file.path, nil, :ignore)
	  when ".xls" then Roo::Excel.new(file.path, nil, :ignore)
	  when ".xlsx" then Roo::Excelx.new(file.path, nil, :ignore)
	  else raise "Unknown file type: #{file.original_filename}"
	  end
	end
  


  def self.export_to_spreadsheet
    authorizer = Google::Auth::ServiceAccountCredentials.make_creds(json_key_io: File.open('config/Dashboard Data.json'),scope: Google::Apis::SheetsV4::AUTH_SPREADSHEETS)
    authorizer.fetch_access_token!
    @sheet_service = Google::Apis::SheetsV4::SheetsService.new
    @sheet_service.authorization = authorizer
    request_body = Google::Apis::SheetsV4::ValueRange.new
    spreadsheet_id = "1GShMfgWmRcYXv1Fhw93OuXy5kfDu4KrEdUH7QfqHSzI"
    sheet_name = "teachers_from_database"
    request_body.major_dimension = "ROWS"
    
    request_body.values = get_information
    request_body.range = "#{sheet_name}!A1:Z#{get_information.length}"
    sheet_response = @sheet_service.update_spreadsheet_value(spreadsheet_id,request_body.range,request_body,value_input_option:"USER_ENTERED")
  end

  def self.get_information
    array = [(Teacher.column_names)]
    all.current_teachers.each do |teacher|
      array.push(Teacher.column_names.map {|attr| teacher.send(attr)})
    end
    array
  end

  def self.get_contact_info
    array = [["id","full_name","phone","email"]]
    all.current_teachers.each do |teacher|
      array.push(Teacher.column_names.map {|attr| teacher.send(attr)})
    end
    array
  end

  
  def self.create_spreadsheet(user,title,info_type)
    @sheet_service = Google::Apis::SheetsV4::SheetsService.new
    @sheet_service.authorization = user.token
    spreadsheet_request_body = Google::Apis::SheetsV4::Spreadsheet.new
    spreadsheet_request_body.update!(properties:{title:"#{title} #{Date.today}"})
    response = @sheet_service.create_spreadsheet(spreadsheet_request_body)
    request_body = Google::Apis::SheetsV4::ValueRange.new
    spreadsheet_id = response.spreadsheet_id
    sheet_name = "Sheet1"
    request_body.major_dimension = "ROWS"
    if info_type == "contact"
      request_body.values = get_contact_info
      request_body.range = "#{sheet_name}!A1:Z#{get_information.length}"
    else 
      request_body.values = get_information
      request_body.range = "#{sheet_name}!A1:Z#{get_information.length}"
    end    
    sheet_response = @sheet_service.update_spreadsheet_value(spreadsheet_id,request_body.range, request_body,value_input_option:"USER_ENTERED")
  rescue Google::Apis::AuthorizationError
    
  end


end
