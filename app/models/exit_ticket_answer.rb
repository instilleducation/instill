class ExitTicketAnswer < ApplicationRecord
  belongs_to :question
  belongs_to :in_person


  scope :programme, -> (programme) {joins(in_person: :programme_session).where(in_people: {programme_sessions:{programme: programme}})}



  def self.export_to_spreadsheet
    authorizer = Google::Auth::ServiceAccountCredentials.make_creds(json_key_io: File.open('config/Dashboard Data.json'),scope: Google::Apis::SheetsV4::AUTH_SPREADSHEETS)
    authorizer.fetch_access_token!
    @sheet_service = Google::Apis::SheetsV4::SheetsService.new
    @sheet_service.authorization = authorizer
    request_body = Google::Apis::SheetsV4::ValueRange.new
    spreadsheet_id = "1AbhW_IIkddlFXvv8d8xM3Q2l31DMzSTKI3UkjiDgP_Y"
    sheet_name = "exit_ticket_answers"
    request_body.major_dimension = "ROWS"
    
    request_body.values = get_information
    request_body.range = "#{sheet_name}!A1:Z#{get_information.length}"
    sheet_response = @sheet_service.update_spreadsheet_value(spreadsheet_id,request_body.range,request_body,value_input_option:"USER_ENTERED")
  end

  def self.get_information
    array = [["id","Question Text","Percentage Correct", "Session Name", "School Name","Date"]]
    all.each do |exit_ticket_answer|
      array.push([exit_ticket_answer.id, exit_ticket_answer.question.question_text, exit_ticket_answer.percentage_correct, exit_ticket_answer.in_person.name, exit_ticket_answer.in_person.programme_session.school.name, exit_ticket_answer.in_person.date_done])
    end
    array
  end 
      

end
