class Section < ApplicationRecord
  belongs_to :programme
  belongs_to :category
end
