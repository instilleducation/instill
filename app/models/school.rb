class School < ApplicationRecord
	has_many :teachers, dependent: :destroy
	has_many :developments, dependent: :destroy
	has_many :programmes, through: :developments
	has_many :programme_sessions, dependent: :destroy
	has_many :conversations, dependent: :destroy
	accepts_nested_attributes_for :conversations, allow_destroy: true
	
    def self.import(file)
	  spreadsheet = Roo::Spreadsheet.open(file.path)
	  header = spreadsheet.row(1)
	  (2..spreadsheet.last_row).each do |i|
	    row = Hash[[header, spreadsheet.row(i)].transpose]
	    product = find_by_id(row["id"]) || new
	    product.attributes = row.to_hash
	    product.save!
  		end
	end

	def self.open_spreadsheet(file)
	  case File.extname(file.original_filename)
	  when ".csv" then Roo::CSV.new(file.path, nil, :ignore)
	  when ".xls" then Roo::Excel.new(file.path, nil, :ignore)
	  when ".xlsx" then Roo::Excelx.new(file.path, nil, :ignore)
	  else raise "Unknown file type: #{file.original_filename}"
	  end
	end

	def previous_school
	  self.class.where(["id < ?", id]).last	end

	def next_school
	  self.class.where(["id > ?", id]).first
	end



end
