class Category < ApplicationRecord
  has_many :sections, dependent: :destroy  
  has_many :programmes, through: :sections, dependent: :destroy
  has_many :criteria , dependent: :destroy
  has_many :levers , dependent: :destroy
end
