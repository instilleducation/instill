class Lever < ApplicationRecord
  belongs_to :category, inverse_of: :levers
  belongs_to :feedback, inverse_of: :levers
end
