class Submission < ApplicationRecord
  belongs_to :self_work
  belongs_to :teacher
  belongs_to :school
  has_many :assignment_grades
  accepts_nested_attributes_for :assignment_grades, allow_destroy: true
  validates :teacher_id, uniqueness: {scope: :self_work_id}

  scope :programme, -> (programme) {joins(self_work: :programme_session).where(self_work: {programme_sessions:{programme: programme}})}
  
    def self.to_csv(options = {})
   		attributes = ["id", "teacher", "school", "session_id", "percentage_complete", "time_taken"]
   		CSV.generate(headers: true) do |csv|
   			csv << attributes

   			all.each do |submission|
   				row = [submission.id, submission.teacher.full_name, submission.school.name,
   					submission.self_work.id, submission.percentage_complete, submission.time_taken]
   				csv << row
   			end
   		end
   	end  


  	def self.import(file,self_work_id)
   		spreadsheet = Roo::Spreadsheet.open(file)
   		header = spreadsheet.row(1)
   		(2..spreadsheet.last_row).each do |i|
        	row = Hash[[header, spreadsheet.row(i)].transpose]
          teacher = Teacher.find_by(full_name: row["teacher"]) || teacher = Teacher.find_by(full_name:"Unknown")
          session = SelfWork.find(self_work_id)
          submission = Submission.find_or_create_by(teacher: teacher, self_work: session, school:teacher.school)
          submission.update_attributes(percentage_complete: row["percentage_complete"], time_taken: row["time_taken"])

      end
    end

	def self.open_spreadsheet(file)
	  case File.extname(file.original_filename)
	  when ".csv" then Roo::CSV.new(file.path, nil, :ignore)
	  when ".xls" then Roo::Excel.new(file.path, nil, :ignore)
	  when ".xlsx" then Roo::Excelx.new(file.path, nil, :ignore)
	  else raise "Unknown file type: #{file.original_filename}"
	  end
	end
	

end