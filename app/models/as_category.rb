class AsCategory < ApplicationRecord
  belongs_to :action_step,optional: true
  belongs_to :takeaway

    
  scope :programme, -> (programme) {joins(takeaway: :feedback).where(takeaways: 
  	{feedbacks:{:observation_done => programme.start_date..programme.end_date}})}

  def self.export_to_spreadsheet
    authorizer = Google::Auth::ServiceAccountCredentials.make_creds(json_key_io: File.open('config/Dashboard Data.json'),scope: Google::Apis::SheetsV4::AUTH_SPREADSHEETS)
    authorizer.fetch_access_token!
    @sheet_service = Google::Apis::SheetsV4::SheetsService.new
    @sheet_service.authorization = authorizer
    request_body = Google::Apis::SheetsV4::ValueRange.new
    spreadsheet_id = "1AbhW_IIkddlFXvv8d8xM3Q2l31DMzSTKI3UkjiDgP_Y"
    sheet_name = "as_categories"
    request_body.major_dimension = "ROWS"
    
    request_body.values = get_information
    request_body.range = "#{sheet_name}!A1:Z#{get_information.length}"
    sheet_response = @sheet_service.update_spreadsheet_value(spreadsheet_id,request_body.range,request_body,value_input_option:"USER_ENTERED")
  end

  def self.get_information
    array = [["id","Name","Action Step Name","School Name","Date"]]
    all.each do |as_category|
      array.push([as_category.id,as_category.name,as_category.action_step.as_name,as_category.takeaway.feedback.teacher.school.name,as_category.takeaway.feedback.observation_done])
    end
    array
  end  
end


      