class Observation < ApplicationRecord
	has_one :programme_session,:as => :profile,dependent: :destroy
	has_many :categories, through: :sections
	has_many :feedbacks, dependent: :destroy
	has_many :scores, through: :feedbacks
	has_many :takeaways, through: :feedbacks
	has_many :criteria, through: :categories




	
end
