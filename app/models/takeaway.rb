class Takeaway < ApplicationRecord
	belongs_to :feedback, inverse_of: :takeaway, optional: true
  	has_many :as_categories,dependent: :destroy
	has_many :action_steps, through: :as_categories,dependent: :destroy
	accepts_nested_attributes_for :as_categories,allow_destroy: true
end
