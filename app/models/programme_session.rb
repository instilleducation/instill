class ProgrammeSession < ApplicationRecord
	belongs_to :programme
	belongs_to :profile, :polymorphic => true, dependent: :destroy
	belongs_to :school

	def self.to_csv(options = {})
   		attributes = ProgrammeSession.column_names
   		CSV.generate(headers: true) do |csv|
   			csv << attributes

   			all.each do |session|
   				row = [session.id, session.programme.name, session.profile.name, 
   					session.profile_type, session.school.name]
   				csv << row
   			end
   		end
   	end

   	def self.import(file)
	  spreadsheet = Roo::Spreadsheet.open(file.path)
	  header = spreadsheet.row(1)
	  (2..spreadsheet.last_row).each do |i|
	    row = Hash[[header, spreadsheet.row(i)].transpose]
	    product = find_by_id(row["id"]) || new
	    product.attributes = row.to_hash
	    product.save!
  		end
	end

	def self.open_spreadsheet(file)
	  case File.extname(file.original_filename)
	  when ".csv" then Roo::CSV.new(file.path, nil, :ignore)
	  when ".xls" then Roo::Excel.new(file.path, nil, :ignore)
	  when ".xlsx" then Roo::Excelx.new(file.path, nil, :ignore)
	  else raise "Unknown file type: #{file.original_filename}"
	  end
	end
end
