class Question < ApplicationRecord
  belongs_to :exit_ticket
  has_many :exit_ticket_answers,   dependent: :destroy
end
