class Criterium < ApplicationRecord
  belongs_to :category
  has_many :scores, inverse_of: :criterium

  scope :category, ->(category) {where(category: category )}
  scope :criterium, ->(criterium) {where(id: criterium)}

    def self.to_csv(options = {})
   		attributes = Criterium.column_names
   		CSV.generate(headers: true) do |csv|
   			csv << attributes

   			all.each do |user|
   				csv << attributes.map {|attr| user.send(attr)}
   			end
   		end
   	end

   	def self.import(file,category_id)
	  spreadsheet = Roo::Spreadsheet.open(file.path)
	  header = spreadsheet.row(1)
	  @category = category_id

	  (2..spreadsheet.last_row).each do |i|
	    row = Hash[[header, spreadsheet.row(i)].transpose]
	    product = find_by(name: row["name"]) || new
	    product.update_attributes(name: row["name"],category:@category ,
	    	focus_points: row["focus_points"], 
	    	possible_score: row["possible_score"])
	    product.save!
  		end
	end

	def self.open_spreadsheet(file)
	  case File.extname(file.original_filename)
	  when ".csv" then Roo::CSV.new(file.path, nil, :ignore)
	  when ".xls" then Roo::Excel.new(file.path, nil, :ignore)
	  when ".xlsx" then Roo::Excelx.new(file.path, nil, :ignore)
	  else raise "Unknown file type: #{file.original_filename}"
	  end
	end
end
