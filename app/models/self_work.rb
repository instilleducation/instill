class SelfWork < ApplicationRecord
	has_one :programme_session,:as => :profile,dependent: :destroy
	has_many :submissions
	has_many :assignments

	scope :programme, -> (programme) {joins(programme_session: :programme).where(programme_sessions: {programme: programme})}
	scope :school, -> (school) {joins(programme_session: :school).where(programme_sessions:{school:school})}
end

