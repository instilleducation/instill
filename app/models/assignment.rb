class Assignment < ApplicationRecord
  belongs_to :self_work
  has_many :assignment_grade
end
