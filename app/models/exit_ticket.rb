class ExitTicket < ApplicationRecord
	has_many :in_people
	has_many :questions,  dependent: :destroy
	has_many :exit_ticket_answers, through: :questions
end
