
class User < ApplicationRecord
	before_save {self.email = email.downcase}
	validates :name, presence: true

	VALID_EMAIL_REGEX = /\A[\w+\-.]+@(instill.education)+\z/i
	validates :email, presence: true, format:{ with: VALID_EMAIL_REGEX }
	has_secure_password
	validates :password, presence: true, length:{minimum: 6}


	def User.digest(string)
    	cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    	BCrypt::Password.create(string, cost: cost)
  	end

  	def refresh_token_if_expired
  		if token_expired?
  			oauth = OmniAuth::Strategies::GoogleOauth2.new(
      			nil, # App - nil seems to be ok?!
      			"31729569911-rsp119k9ifjkcm60o2l7g3t2i8a30rri.apps.googleusercontent.com", # Client ID
      			"QKoZZhBsckdIwsIlPA_hn-81" # Client Secret
    		)
    	g_token = OAuth2::AccessToken.new(
      	oauth.client,
      	self.token,
      	{ refresh_token: self.token }
    	)
    	new_token = g_token.refresh!

    	if new_token.present?
	      	self.update(
	        token: new_token.token,
	        expires_at: new_token.expires_at,
	        refresh_token: new_token.refresh_token)	    	
  		end
  	end
	end

	def token_expired?
	  expiry = Time.at(self.expires_at.to_i) 
	  return true if expiry < Time.now # expired token, so we should quickly return
	  token_expires_at = expiry
	  save if changed?
	  false # token not expired. :D
	end



end

oauth = OmniAuth::Strategies::GoogleOauth2.new(
nil,
"31729569911-rsp119k9ifjkcm60o2l7g3t2i8a30rri.apps.googleusercontent.com", # Client ID
"QKoZZhBsckdIwsIlPA_hn-81"
)