class Score < ApplicationRecord
  belongs_to :feedback, inverse_of: :scores
  belongs_to :criterium, inverse_of: :scores
  
  scope :criterium, ->(criterium) {where(criterium: criterium )}
  scope :category, -> (category) {joins(:criterium).where(criteria: {category_id: category})}
  scope :school, -> (school) {joins(:feedback => :teacher).where(feedbacks: {teachers: {school_id:school}})}
  scope :observer, -> (observer) {joins(:feedback).where(feedbacks: {user_id: observer})}
  scope :observation_order, -> (observation_order) {joins(feedback: :observation).where(feedbacks: {observations: {observation_order:observation_order}})}
  scope :phase, -> (phase) {joins(:feedback => :teacher).where(feedbacks:{teachers: {grade: phase}})}
  scope :programme, ->(programme) {joins(criterium: :category).where(criteria: {categories:{programme_id: programme}})}


  def self.export_to_spreadsheet
    authorizer = Google::Auth::ServiceAccountCredentials.make_creds(json_key_io: File.open('config/Dashboard Data.json'),scope: Google::Apis::SheetsV4::AUTH_SPREADSHEETS)
    authorizer.fetch_access_token!
    @sheet_service = Google::Apis::SheetsV4::SheetsService.new
    @sheet_service.authorization = authorizer
    request_body = Google::Apis::SheetsV4::ValueRange.new
    spreadsheet_id = "1AbhW_IIkddlFXvv8d8xM3Q2l31DMzSTKI3UkjiDgP_Y"
    sheet_name = "scores"
    request_body.major_dimension = "ROWS"
    
    request_body.values = get_information
    request_body.range = "#{sheet_name}!A1:Z#{get_information.length}"
    sheet_response = @sheet_service.update_spreadsheet_value(spreadsheet_id,request_body.range,request_body,value_input_option:"USER_ENTERED")
  end

  def self.get_information
    array = [["id","Instruction Technique","Instruction Area","Score Value","Observation Name","School Name","Date"]]
    all.each do |score|
      array.push([score.id,score.criterium.name,score.criterium.category.name,score.score_value,score.feedback.observation.name,score.feedback.teacher.school.name,score.feedback.observation_done])
    end
    array
  end   
  


end
