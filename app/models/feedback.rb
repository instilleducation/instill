require 'google/apis/script_v1'
require 'googleauth'
class Feedback < ApplicationRecord
  belongs_to :teacher, optional: true
  belongs_to :user, optional: :true
  belongs_to :observation
  has_one :takeaway, dependent: :destroy, inverse_of: :feedback
  has_many :levers, dependent: :destroy, inverse_of: :feedback
  has_many :scores , inverse_of: :feedback, dependent: :destroy
  accepts_nested_attributes_for :levers, allow_destroy: true
  accepts_nested_attributes_for :takeaway, allow_destroy: true
  accepts_nested_attributes_for :scores, allow_destroy: true

  validates :teacher_id, uniqueness: {scope: :observation_id}

	def self.import(file_data,observation_id)
	  		@observation = Observation.find(observation_id)
	  		#find the correct observation
	  		@school= @observation.programme_session.school
	  		#create a feedback object populate with observation details and precise positives
	  		details = file_data["observation_details"]
	  		teacher = @school.teachers.fuzzy_search(full_name: details["teacher"]).first || teacher = nil
	  		coach = User.fuzzy_search(name: details["coach"]).first || coach = nil
	  		
	  		pp = file_data["precise_positives"]
	  		takeaway = file_data["biggest_takeaway"]
	  		levers = file_data["levers"]
	  		learner_engagement = file_data["learner_engagement"]
	  		scores = file_data["scores"].flatten(1)
	  		
	  		@programme = @observation.programme_session.programme
	  		if(!pp.nil?)
	  			@feedback = Feedback.new(observation_done: details["date"], teacher: teacher, user:coach ,
	  			pp_effective_1: pp["effective1"],pp_effective_2: pp["effective2"],pp_impact_1:pp["impact1"] ,pp_impact_2:pp["impact2"])
	  			#first_ontask:learner_engagement["first_ontask"],second_ontask:learner_engagement["second_ontask"],first_offtask:learner_engagement["first_offtask"],second_offtask:learner_engagement["second_offtask"])
	  		else
	  			@feedback = Feedback.new(observation_done: details["date"], teacher:teacher, user: coach)
	  		end 
	  		#for all score
	  			#observations find critera by name
	  			#create score using the critera found above a the score and score notes in the file
	  		scores.each do |score|
	  			@criterium = @programme.criteria.find_by(name:score["criterion"])
	  			if(!@criterium.nil? && !score["score"].nil?)
	  				@feedback.scores.new(criterium: @criterium,score_value: score["score"],score_notes: score["score_notes"])
	  			end
	  		end
			#create a takeaway object using 
	  		#what_happened, and prep action_step_ids will be one and edited by hand
	  		if(!takeaway.nil?)
	  			@feedback.build_takeaway
	  			takeaway.each do |action_step|
	  				@feedback.takeaway.as_categories.new(name:action_step["category"])
	  			end
	  		else
	  			@feedback.build_takeaway
	  			2.times{@feedback.takeaway.as_categories.new}
	  		end
	  		@feedback.save
	  		return @feedback
		end

	def self.google_file(user,fileURL,categories)
		@script_service = Google::Apis::ScriptV1::ScriptService.new
		@script_service.authorization = user.token
		request = Google::Apis::ScriptV1::ExecutionRequest.new(
				function: 'createJSON',
				parameters: [fileURL,categories]
				)

		reps = @script_service.run_script("1vbKDtIOs2HvZSV4gjw1dHd3Gw8l9MfsWPkIjSMwd_VlcWRw0PzkYJe8k",request)
		
		feedback = JSON.parse(reps.response["result"])
		return feedback
	end

	def self.JSON_file(file)
	   	fileN = File.new(file.path)
	   	files = File.read(fileN)
	  	file_data = JSON.parse(files)
	end

	




	def self.to_csv(options = {})
		@criteria = Criterium.order(:id)

 		attributes = ["teacher","school","observer","observation_number"]
 		@criteria.each do |criterium|
 			attributes.push("#{criterium.category.name} : #{criterium.name}")
 		end

 		CSV.generate(headers: true) do |csv|
 			csv << attributes		

			all.each do |feedback|
				row = [feedback.teacher.full_name,feedback.teacher.school.name,feedback.user.name, 
					feedback.observation.observation_order]

					@criteria.each do |criterium|
						score = feedback.scores.find_by(criterium: criterium)
						if !score.nil?
							row.push(score.score_value)
						else
							row.push("-")

						end
					end

				csv <<row
			end
		end
	end
    
	


end
