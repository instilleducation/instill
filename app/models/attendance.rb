class Attendance < ApplicationRecord
  belongs_to :in_person
  belongs_to :teacher
  belongs_to :school
  validates :teacher_id, uniqueness: {scope: :in_person_id}

  scope :programme, -> (programme) {joins(in_person: :programme_session).where(in_people: {programme_sessions:{programme: programme}})
}

  def self.to_csv(options = {})
 		attributes = ["id","teacher","school","session_id","attendance_value"]
 		CSV.generate(headers: true) do |csv|
 			csv << attributes

 			all.each do |attendance|
 				row = [attendance.id, attendance.teacher.full_name, attendance.school.name,
          attendance.in_person_id, attendance.attendance_value]
 				csv << row
 			end
 		end
  end

  def self.import(file)
   		spreadsheet = Roo::Spreadsheet.open(file)
   		header = spreadsheet.row(1)
   		(2..spreadsheet.last_row).each do |i|
        row = Hash[[header, spreadsheet.row(i)].transpose]
        teacher = Teacher.find_by(full_name: row["teacher"])
        school = teacher.school
        session = InPerson.find(row["session_id"])
        attendance = find_by(teacher: teacher, in_person: session) || new
        attendance.update_attributes(attendance_value: row["attendance_value"])
      end
	end

	def self.open_spreadsheet(file)
	  case File.extname(file.original_filename)
	  when ".csv" then Roo::CSV.new(file.path, nil, :ignore)
	  when ".xls" then Roo::Excel.new(file.path, nil, :ignore)
	  when ".xlsx" then Roo::Excelx.new(file.path, nil, :ignore)
	  else raise "Unknown file type: #{file.original_filename}"
	  end
	end

  def self.export_to_spreadsheet
    authorizer = Google::Auth::ServiceAccountCredentials.make_creds(json_key_io: File.open('config/Dashboard Data.json'),scope: Google::Apis::SheetsV4::AUTH_SPREADSHEETS)
    authorizer.fetch_access_token!
    @sheet_service = Google::Apis::SheetsV4::SheetsService.new
    @sheet_service.authorization = authorizer
    request_body = Google::Apis::SheetsV4::ValueRange.new
    spreadsheet_id = "1AbhW_IIkddlFXvv8d8xM3Q2l31DMzSTKI3UkjiDgP_Y"
    sheet_name = "attendances"
    request_body.major_dimension = "ROWS"
    
    request_body.values = get_information
    request_body.range = "#{sheet_name}!A1:Z#{get_information.length}"
    sheet_response = @sheet_service.update_spreadsheet_value(spreadsheet_id,request_body.range,request_body,value_input_option:"USER_ENTERED")
  end

  def self.get_information
    array = [["id","Attendace Value","Session Name","Teacher Name","Teacher Role","School Name","Number of Teachers","Session Date"]]
    all.each do |attendance|
      array.push([attendance.id, attendance.attendance_value, attendance.in_person.name, attendance.teacher.full_name, attendance.teacher.role, attendance.teacher.school.name, attendance.in_person.expected_attendance, attendance.in_person.date_done])
    end
    array
  end   

  
end

        