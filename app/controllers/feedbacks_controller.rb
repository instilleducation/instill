class FeedbacksController < ApplicationController
	before_action :find_as_observation, only:[:new,:create,:import,:edit,:update,:destroy,:add_lever,:add_score,:import_google]
	before_action :logged_in_user
	def new
		@programme = @observation.programme_session.programme
		@feedback = @observation.feedbacks.new()
		#@programme.categories.count.times{@feedback.levers.new}
		@feedback.build_takeaway
		2.times{@feedback.takeaway.as_categories.new}
		@programme.criteria.count.times{@feedback.scores.new}
		
		#for i in 0..@programme.categories.count-1 do 
		#	@feedback.levers[i].category = @programme.categories[i]
		#end

		for i in 0..@programme.criteria.count-1 do
			@feedback.scores[i].criterium = @programme.criteria[i]
		end

	end

	def create
		@programme = @observation.programme_session.programme

		@feedback = @observation.feedbacks.new(feedbacks_params)
		#for i in 0..@programme.categories.count-1 do 
		#	@feedback.levers[i].category = @programme.categories[i]
		#end

		for i in 0..@programme.criteria.count-1 do
			@feedback.scores[i].criterium = @programme.criteria[i]
		end
		
		if @feedback.save
				
				redirect_to @observation
				flash[:success] = "Feedback successfully created"
		
		else
			redirect_to @observation 
			
			flash[:danger] = "Feedback Failed"
		end


	end

	

	def import
		@json_feedback = Feedback.JSON_file(params[:file])
		@feedback = @observation.feedbacks.import(@json_feedback,params[:observation_id])
		redirect_to edit_observation_feedback_path(@observation,@feedback)
	end

    def import_google
    	@programme =@observation.programme_session.programme 
    	user = User.find(session["user_id"])
    	if user.token.nil? || user.token_expired?
    		store_location
    		redirect_to '/auth/google_oauth2'
    	else
    		@json_feedback = Feedback.google_file(current_user,params[:url],params[:programme][:categories])
			@feedback = @observation.feedbacks.import(@json_feedback,params[:observation_id])
			redirect_to edit_observation_feedback_path(@observation,@feedback)
		end
		
    end

	def edit
		@feedback = @observation.feedbacks.find(params[:id])
	end

	def update
		@feedback = @observation.feedbacks.find(params[:id])
		
		if @feedback.update_attributes(feedbacks_params)
			
			redirect_to observation_feedback_path(@observation,@feedback)
			flash[:success] = "Feedback successfully updated"
		else
			redirect_to observation_feedback_path(@observation,@feedback)
			
			flash[:danger] = "Feedback is not successfully updated"
		end
	end

	def show
		@observation = Observation.find(params[:observation_id])
		@programme = @observation.programme_session.programme
		@feedback = @observation.feedbacks.find(params[:id])
	end


	def destroy
		@feedback = @observation.feedbacks.find(params[:id])
		@feedback.destroy
		flash[:success] = "Table Deleted"
    	redirect_to @observation
    end

    def add_lever
    	@programme = @observation.programme_session.programme
    	@feedback = @observation.feedbacks.find(params[:feedback_id])
    	@lever = @feedback.levers.create(category: @programme.categories.find(params[:category]),
    			highest_lever: params[:highest_lever])
    	redirect_to observation_feedback_path(@observation, @feedback)
    end

    def add_score
    	@programme = @observation.programme_session.programme
    	@feedback = @observation.feedbacks.find(params[:feedback_id])
    	@lever = @feedback.scores.create(criterium: @programme.criteria.find(params[:criterium]),
    		score_value: params[:score_value], score_notes: params[:score_notes])

    	redirect_to observation_feedback_path(@observation, @feedback)
    end




	private 

	def find_as_observation
		@observation = Observation.find(params[:observation_id])
	end

	def feedbacks_params
		params.require(:feedback).permit(:teacher_id, :observation_id,:observation_done, :user_id, :pp_effective_1,:pp_effective_2,:pp_impact_1 ,:pp_impact_2, :first_offtask, :first_ontask, :second_offtask, :second_ontask,
			levers_attributes: [:id,:highest_lever], 
			takeaway_attributes: [:id, :what_happened, :action_step_id, :prep, as_categories_attributes: [:id, :name,:action_step_id]],
			scores_attributes: [:id, :score_value, :score_notes])
	end


	def teacher_exists?(teacher_id)
  		@observation.feedbacks.exists?(:teacher_id => teacher_id)
  	end




end
