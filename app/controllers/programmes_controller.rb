class ProgrammesController < ApplicationController
    before_action :logged_in_user
    before_action :find_as_programme, only:[:edit,:show,:update,:destroy]
  def new
    @programme = Programme.new
  end

  def edit
  end

  def create
    @programme = Programme.new(programme_params)
    if @programme.save
      flash[:success] = "Programme Created"
      redirect_to @programme
    else
      flash[:danger] = "Programme hasn't been created"
      redirect_to 'new'
    end
  end

  def show
    @categories = @programme.categories
    @criteria = @programme.criteria
  end

  def index
    @programmes = Programme.all
  end

  def destroy
    if @programme.destroy
      flash[:success] = "Programme Deleted"
    else
      flash[:danger] = "Programme is not deleted"
    end
      redirect_to programmes_path
  end

  def update
    if @programme.update_attributes(programme_params)
      redirect_to @programme
    else
      render 'edit'
    end
  end

  def update_spreadsheet
    @programme = Programme.find(params[:programme_id])
    @as_categories = AsCategory.programme(@programme)
    @in_people = InPerson.programme(@programme)
    @scores = Score.programme(@programme)
    @conversations = Conversation.programme(@programme)
    @exit_ticket_answers = ExitTicketAnswer.programme(@programme)
    @attendances = Attendance.programme(@programme)

    @as_categories.export_to_spreadsheet
    @in_people.export_to_spreadsheet 
    @scores.export_to_spreadsheet 
    @conversations.export_to_spreadsheet 
    @exit_ticket_answers.export_to_spreadsheet 
    @attendances.export_to_spreadsheet 

    redirect_to @programme
  end



  private

  def find_as_programme
    @programme = Programme.find(params[:id])
  end

  def programme_params
    params.require(:programme).permit(:name, :start_date,:end_date, school_ids:[])
  end
end 
 