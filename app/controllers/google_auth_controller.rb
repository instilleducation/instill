class GoogleAuthController < ApplicationController
	def create
  		@auth = request.env['omniauth.auth'] #here not found 'omniath.auth' and @auth is nil
	    if @auth.nil?
	      flash[:danger] = "Error during connection with Google API."
	      redirect_to root_path
	    else
	      	@user = User.find(session["user_id"])
	      	@user.update_columns(token: @auth['credentials']['token'],
	      		refresh_token: @auth['credentials']['refresh_token'],
	      		expires_at: @auth['credentials']['expires_at'])
	      	redirect_back_or users_url
	    end
  	end
end



