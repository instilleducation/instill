class ScoresController < ApplicationController
  before_action :logged_in_user
  def index
  	@scores = Score.where(nil).paginate(per_page:10 ,page: params[:page])
  	@scores = @scores.criterium(params[:criterium]) if params[:criterium].present?
  	@scores = @scores.category(params[:category]) if params[:category].present?
  	@scores = @scores.school(params[:school]) if params[:school].present?
  	@scores = @scores.observation_order(params[:observation_number]) if params[:observation_number].present?
  	@scores = @scores.phase(params[:teacher_phase]) if params[:teacher_phase].present?
  	@scores = @scores.observer(params[:observer]) if params[:observer].present?

  	@all_scores = Score.where(nil)
  	@all_scores = @all_scores.criterium(params[:criterium]) if params[:criterium].present?
  	@all_scores = @all_scores.category(params[:category]) if params[:category].present?
  	@all_scores = @all_scores.school(params[:school]) if params[:school].present?
  	@all_scores = @all_scores.observation_order(params[:observation_number]) if params[:observation_number].present?
  	@all_scores = @all_scores.phase(params[:teacher_phase]) if params[:teacher_phase].present?
  	@all_scores = @all_scores.observer(params[:observer]) if params[:observer].present?


  end



end
