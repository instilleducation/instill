class SessionsController < ApplicationController
	include SessionsHelper
  def new
  end

  def create
  	user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
    	log_in user
      flash[:success] = "Welcome back #{user.name}"
    	redirect_back_or static_pages_dashboard_path
    else
    	flash[:danger] = "Invalid email/password combination"
    	render 'new'
    end
  end

  def destroy
  	user = User.find(session["user_id"])
    user.update_attribute(:token, nil)
    log_out
    redirect_to root_url
  end
  
end
