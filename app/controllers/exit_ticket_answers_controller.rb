class ExitTicketAnswersController < ApplicationController
	before_action :logged_in_user

	def edit_all
		@in_person = InPerson.find(params[:in_person_id])
		@exit_ticket = @in_person.exit_ticket
		@exit_ticket_answers = @in_person.exit_ticket_answers		
	end

	def update_all
		@in_person = InPerson.find(params[:in_person_id])
		@exit_ticket = @in_person.exit_ticket
		ExitTicketAnswer.update(params[:answers].keys,params[:answers].values)
		redirect_to @in_person 	
	end


	private
	def exit_ticket_answer_params
		params.require(:exit_ticket_answer).permit(:percentage_correct)
	end
	

end

