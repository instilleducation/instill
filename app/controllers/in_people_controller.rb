class InPeopleController < ApplicationController
before_action :logged_in_user
before_action :find_as_in_person, only: [:edit,:update,:show,:destroy,:edit_exit_ticket, :create_mulitple_attendances]
  
  def new
    @in_person = InPerson.new
  end

  def create
    @in_person = InPerson.new(in_person_params)

    if @in_person.save
      redirect_to @in_person, notice: "Session successfully created."
    end
  end


  def edit
    @in_person=InPerson.find(params[:id])
  end

  def update
    if @in_person.update_attributes(in_person_params)
      create_attendances_for_school
      redirect_to @in_person , notice: "Session has been updated successfully"
    end
  end


  def show
    @attendances = @in_person.attendances.includes(:teacher).order("teachers.full_name asc")
    @school = @in_person.programme_session.school
    @questions = @in_person.questions
    @exit_ticket_answers = ExitTicketAnswer.where(in_person:@in_person)
  end


  def index
    @in_people = InPerson.order("created_at DESC")
  end

  def destroy
    @in_person.destroy
    redirect_to programme_sessions_url
  end

  def add_exit_ticket
    @in_person = InPerson.find(params[:in_person_id])
    if !params[:exit_ticket_id].blank?
      @exit_ticket = ExitTicket.find(params[:exit_ticket_id])
      @exit_ticket.in_people << @in_person
      create_question_scores
      redirect_to @in_person
    else
      redirect_to @in_person
      flash[:danger] = "You cannot input an empty exit ticket"
    end
    
  end


  def create_attendance
    @in_person = InPerson.find(params[:in_person_id])
    teacher = Teacher.find(params[:teacher_id])
    @school = @in_person.programme_session.school
    @in_person.attendances.create(teacher: teacher, school: teacher.school)
    redirect_to @in_person
  end


  private

  def find_as_in_person
    @in_person = InPerson.find(params[:id])
  end

  def in_person_params
    params.require(:in_person).permit(:name, :date_done,:scheduled_date,:in_person_type,:expected_attendance,:scheduled_duration, :actual_duration)
  end

  def create_question_scores
    if !ExitTicketAnswer.where(in_person:@in_person).empty?
      ExitTicketAnswer.where(in_person:@in_person).destroy_all
    end

    @exit_ticket = ExitTicket.find(params[:exit_ticket_id])
    @exit_ticket.questions.all.each do|question|
      ExitTicketAnswer.create(in_person:@in_person,
        question:question)
    end    
  end

  def teacher_exists?(teacher_id)
    @in_person.attendances.exists?(:teacher_id => teacher_id)
  end

  def create_attendances_for_school
    if(!@in_person.programme_session.nil?)
      @school = @in_person.programme_session.school
      @teachers = @school.teachers.where.not(status:"Left School")
      
      @teachers.each do |teacher|
        if !teacher_exists?(teacher.id)
          @in_person.attendances.create(teacher: teacher, school: @school)
        end
      end
    end
  end



  
end
