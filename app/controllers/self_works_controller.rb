class SelfWorksController < ApplicationController
    before_action :logged_in_user
    before_action :find_as_self_work, only: [:edit,:show,:destroy,:update]
  
  def new
    @self_work = SelfWork.new
  end

  def create
    @self_work = SelfWork.new(self_work_params)
    if @self_work.save 

      flash[:success] = "Session Created"
      redirect_to @self_work
    end
  end

  def edit
  end

  def show
    @submissions = @self_work.submissions

    respond_to do |format|
      format.html
      format.csv{send_data @submissions.to_csv, filename: "#{@self_work.name}_submissions.csv"}
    end
    @assignments = @self_work.assignments
  end

  def index
    @self_works = SelfWork.all
  end


  def destroy
    @self_work.destroy
    flash[:success] = "Session Deleted"
    redirect_to programme_sessions_path
  end

  def update
    if @self_work.update_attributes(self_work_params)
   
      redirect_to @self_work
    end
  end

  private

  def find_as_self_work
    @self_work = SelfWork.find(params[:id])
  end

  def self_work_params
    params.require(:self_work).permit(:name)
  end


  
  


end