require 'googleauth'
require 'googleauth/web_user_authorizer'
require 'googleauth/stores/redis_token_store'
require 'redis'

class TeachersController < ApplicationController
    before_action :logged_in_user

	def new
  		@teacher = Teacher.new
  	end

	def create
	  	@teacher = Teacher.new(teacher_params)
	  	if @teacher.save
	  		flash[:success] = "Teacher Created"
	  		redirect_to @teacher
	  	else
	  		render 'new'
	  	end
  end

  def index
  	@teachers = Teacher.where.not(status:"Left School").paginate(page: params[:page], per_page: 20)
    @all_teachers = Teacher.all
    respond_to do |format|
      format.html
      format.csv {send_data @all_teachers.to_csv, filename: "teachers - #{Date.today}.csv"}
    end
  end

  def show
  	@teacher= Teacher.find(params[:id])
  end

  def import 
    Teacher.import(params[:file])
    redirect_to teachers_url
  end

  def edit
  	@teacher = Teacher.find(params[:id])
  end

  def update
  	@teacher = Teacher.find(params[:id])
  	if @teacher.update_attributes(teacher_params)
  		redirect_to @teacher
  	else
  		render 'edit'
  	end
  end


  def destroy
    Teacher.find(params[:id]).destroy
    flash[:sucess] = "User deleted"
    redirect_to teachers_url
  end


  def to_google_sheet
    @teachers = Teacher.all
    @teachers.export_to_spreadsheet
    redirect_to teachers_path

  end

  def create_sheet
    @teachers = Teacher.current_teachers
    user = User.find(session["user_id"])
    if user.token.nil? || user.token_expired?
      store_location
      redirect_to '/auth/google_oauth2'
    else
      user.refresh_token_if_expired
      @teachers.create_spreadsheet(user,"Teachers","general")
      redirect_to teachers_path
    end
  end




private
  def teacher_params
  	params.require(:teacher).permit(:full_name, :email,:school_id,
      :status,:phone, :id_number, :sace_no, :grade,:subject,:qualification,:university,:role,:gender,:years_of_teaching,:age,:communication)
  end

end


 