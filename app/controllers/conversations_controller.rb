class ConversationsController < ApplicationController
	before_action :find_as_school, only:[:new,:create,:show,:edit,:update,:destroy,:increase_number]
	before_action :logged_in_user
	def new
  		@conversation = @school.conversations.new
  	end


	def create
  		@conversation = @school.conversations.new(conversation_params)
  		if @conversation.save
  			redirect_to @school
  		else
  			flash[:danger] = "Converstion not created" 
  		end

  	end

  	def index
    	@conversations = @school.conversations
      	respond_to do |format|
        	format.html
        	format.csv 
    	end
  	end

	def edit
		@conversation = Conversation.find(params[:id])
	end

	def update
		@conversation = @school.conversations.find(params[:id])
		if @conversation.update_attributes(conversation_params)
			redirect_to @school
		else
			redirect_to @school
		end
	end


	def destroy
		@conversation = @school.conversations.find(params[:id])
		if @conversation.destroy
		  redirect_to @conversation
		else
		  flash[:danger] = "Can't be deleted"
		end
	end



	private

	def conversation_params
		params.required(:conversation).permit(:school_id,:programme_id,:month,:status)
	end

	def find_as_school
  		@school = School.find(params[:school_id])
	end



end
