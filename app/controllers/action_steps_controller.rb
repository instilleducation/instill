class ActionStepsController < ApplicationController
	before_action :find_as_action_step, only:[:show,:edit,:update,:destroy]
	before_action :logged_in_user
	def new
		@action_step = ActionStep.new()
	end

	def create
		@action_step = ActionStep.new(action_step_params)
		if @action_step.save
			flash[:success] = "ActionStep Created"
			redirect_to @action_step
		else
			render 'new'
		end

	end

	def index
		@action_steps = ActionStep.paginate(page: params[:page], :per_page=> 20)
		@all_action_steps = ActionStep.all
		respond_to do |format|
      		format.html
      		format.csv {send_data @all_action_steps.to_csv, filename: "action_steps - #{Date.today}.csv"}
    	end
	end

	def show

	end

	def import 
    	ActionStep.import(params[:file])
    	redirect_to action_steps_path
  	end


	def edit

	end

	def update
		if @action_step.update_attributes(action_step_params)
			redirect_to @action_step
		else
			render 'edit'
		end

	end

	def destroy
		ActionStep.find(params[:id]).destroy
		flash[:success] = "ActionStep deleted"

	end


	private 

	def find_as_action_step
		@action_step = ActionStep.find(params[:id])
	end

	def action_step_params
		params.required(:action_step).permit(:as_name, :as_code)

	end

end
