class SubmissionsController < ApplicationController
	before_action :logged_in_user
	
	def new
		@self_work = SelfWork.find(params[:self_work_id])
		@submission = @self_work.submissions.new
		@self_work.assignments.count.times{@submission.assignment_grades.new}
	end

	def create
		@self_work = SelfWork.find(params[:self_work_id])
		@school = @self_work.programme_session.school
		@submission = @self_work.submissions.create(submissions_params)



		for i in 0..@self_work.assignments.count-1 do 
			@submission.assignment_grades[i].assignment = @self_work.assignments[i]
		end

		if !teacher_exist?(params[:submission]['teacher_id'])
			@submission.update_attributes(school: @school)
			if @submission.save
				redirect_to @self_work
				flash[:success] = "Submission successfully created"
			else
				redirect_to @self_work
				flash[:danger] = "Submission Failed"
			end
		else
			redirect_to @self_work
			flash[:danger] = "Teacher Exists"
		end
	end

	def index
    	@self_work = SelfWork.find(params[:self_work_id])
    	@submissions = @self_work.submissions
      
      	respond_to do |format|
        	format.html
        	format.csv {send_data @submissions.to_csv, filename: "submissions.csv"}
    	end
    end



  	def import
   		@self_work = SelfWork.find(params[:self_work_id])
   		@self_work.submissions.import(params[:file],params[:self_work_id])
    	redirect_to @self_work
    end


	def edit
		@self_work = SelfWork.find(params[:self_work_id])
		@submission = @self_work.submissions.find(params[:id])
	end

	def update
		@self_work = SelfWork.find(params[:self_work_id])
		@submission = @self_work.submissions.find(params[:id])
		if @submission.update_attributes(submissions_params)
			redirect_to @self_work 
			flash[:success] = "Submission has been updated."

		end
	end

	def destroy
		@self_work = SelfWork.find(params[:self_work_id])
		@submission = @self_work.submissions.find(params[:id])
		if @submission.destroy
			redirect_to @self_work
			flash[:success] = "Submission has been deleted"
		else
			flash[:danger] = "Submission hasn't been deleted something wrong happened"
		end
	end



	private 

	def submissions_params
		params.require(:submission).permit(:teacher_id, :percentage_complete, :time_taken, :school,
			assignment_grades_attributes: [:id, :assignment_id, :grade_given])
	end

	def teacher_exist?(teacher)
		@self_work.submissions.exists?(:teacher_id => params["submission"]["teacher_id"])
	end

end
