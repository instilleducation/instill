class AttendancesController < ApplicationController
  before_action :find_as_in_person, only:[:new,:show,:index,:import,:update,:edit,:destroy,:update_multiple,:edit_multiple]
  before_action :logged_in_user
  def new
  	@attendance = @in_person.attendances.new
  end

  def create
 	  @school = @in_person.programme_session.school
  	@attendance = @in_person.attendances.build(attendance_params)
  	
  	if !teacher_exists?(params[:attendance]['teacher_id'])
  		@attendance.update_attributes(school:@school)
  		if @attendance.save
  			redirect_to @in_person
  		end
  	else
  		redirect_to @school, notice: "Teacher exists"
  	end
  end

  def index
    @attendances = @in_person.attendances.includes(:teacher).order("teachers.full_name asc")
      
      respond_to do |format|
        format.html
        format.csv {send_data @attendances.to_csv, filename: "#{@in_person.name}_#{@in_person.programme_session.school.name}_attendances.csv"}
    end
  end


  def import
    Attendance.import(params[:file])
    redirect_to @in_person
  end

  def edit
  	@attendance = @in_person.attendances.find(params[:id])


  end

  def update
  	@attendance = @in_person.attendances.find(params[:id])
  	if @attendance.update_attributes(attendance_params)
  		  respond_to do |format|
        format.html {redirect_to @in_person}
        format.js
        end
  	end
  end

  
  def destroy
    @attendance = @in_person.attendances.find(params[:id])
    if @attendance.destroy
      redirect_to @in_person
    else
      flash[:danger] = "Can't be deleted"
    end
  end

  def edit_multiple
    @attendances = @in_person.attendances.includes(:teacher).order("teachers.full_name asc")
  end

  def update_multiple
    Attendance.update(params[:attendances].keys, params[:attendances].values)
    redirect_to @in_person
  end


  private

  def find_as_in_person
    @in_person = InPerson.find(params[:in_person_id])
  end

  def attendance_params
  	params.require(:attendance).permit(:teacher_id, :attendance_value, :school_id)
  end

  def teacher_exists?(teacher_id)
  	@in_person.attendances.exists?(:teacher_id => teacher_id)
  end

  def has_not_left?(teacher)
    teacher.status != "Left School"
  end

end
