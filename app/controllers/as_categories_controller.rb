class AsCategoriesController < ApplicationController
  before_action :find_as_category, only:[:show,:edit,:update,:destroy]
  before_action :logged_in_user
  def new
  	@as_category = AsCategory.new
  end

  def create
  	@as_category = AsCategory.new(as_category_params)
  	if @as_category.save
  		flash[:success] = "Contact was successfully created"
  		redirect_to as_categories_path
  	else
  		flash[:danger]
  		render 'new'
  	end
  end

  def show

  end

  def index
  	@as_categories = AsCategory.all
  end

  def edit
  	@as_category.update(as_category_params)
  	redirect_to @as_category
  end

  def destroy
  	@as_category.destroy
  	redirect_to as_categories_path
  end

  private
  
  def find_as_category
  	@as_category = @as_category.find(params[:id])
  end

  def as_category_params
  	params.required(:as_categories).permit(:name)
  end
end
