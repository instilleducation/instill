class AssignmentsController < ApplicationController
	before_action :find_as_self_work, only:[:new,:create,:edit,:update,:destroy]
	before_action :logged_in_user
	def new
		@assignment = @self_work.assignments.new
	end

	def create
		@school = @self_work.programme_session.school
		@assignment = @self_work.assignments.build(assignments_params)
		if @assignment.save
			flash[:success] = "Assignment has been created"
			redirect_to @self_work
		end


	end

	def edit
		@assignment = @self_work.assignments.find(params[:id])
	end

	def update
		@assignment = @self_work.assignments.find(params[:id])
		if @assignment.update_attributes(assignments_params)
			redirect_to @self_work, notice: "Submission has been updated."
		end
	end

	def destroy
		@assignment = @self_work.assignments.find(params[:id])
		if @assignment.destroy
			flash[:success] = "Assignment has been destroy"
			redirect_to @self_work
		else
			flash[:danger] = "Assignment hasn't been destroyed"
			redirect_to @self_work
		end
	end


	private 

	def find_as_self_work
		@self_work = SelfWork.find(params[:self_work_id])
	end

	def assignments_params
		params.require(:assignment).permit(:name,:description,:possible_score)
	end



end