class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  private 
    def current_user?(user)
    	user == current_user
  	end


	def current_user
		@current_user ||= User.find_by(id: session[:user_id])
	end

	def logged_in?
		!current_user.nil?
	end

	def log_out
		session.delete(:user_id)
		@current_user=nil
	end


	def redirect_back_or(default)
		redirect_to(session[:forwarding_url] || default)
		session.delete(:forwarding_url)
	end

	def store_location
		session[:forwarding_url] = request.original_url if request.get?
	end

	def user_params
  		params.require(:user).permit(:name, :email, 
  		:password, :password_confirmation)
  	end

  	def logged_in_user
  		unless logged_in?
  			store_location
  			flash[:danger] = "Please log in"
  			redirect_to login_url
  		end
  	end

  	def correct_user
  		@user = User.find(params[:id])
      redirect_to(users_url) unless current_user?(@user) || admin?
      
  	end

  	def admin_user
  		redirect_to(root_url) unless current_user.admin?
      flash[:danger] = "You are not authorised to create user, contact Admin"
    end
    def admin?
      current_user.admin
    end


end
