class ObservationsController < ApplicationController
  before_action :logged_in?, only: [:show,:edit, :update, :index]
  before_action :find_as_observation, only:[:edit,:update,:show,:destroy]
  def new
    @observation = Observation.new
  end

  def create
    @observation = Observation.new(observation_params)
    if @observation.save
      redirect_to @observation, notice: "Session successfully created."
    end
  end


  def edit
    @observation = Observation.find(params[:id])

  end

  def update
    @observation = Observation.find(params[:id])
    if @observation.update_attributes(observation_params)
      redirect_to @observation, notice: "Session has been updated successfully"
    end
  end


  def show

    @observation = Observation.find(params[:id])
     @programme = @observation.programme_session.programme
    @feedbacks = @observation.feedbacks
    respond_to do |format|
        format.html
        format.csv {send_data @feedbacks.to_csv, filename: "#{@observation.name}_#{@observation.programme_session.school.name}_observations.csv"}
    end
    

  end

  def index
    @observations = Observation.all
  end

  def destroy
    @observation.destroy
    redirect_to programme_sessions_path
  end

  private

  def find_as_observation
    @observation = Observation.find(params[:id])
  end

  def observation_params
    params.require(:observation).permit(:name, :done, :observation_type, :observation_order)
  end
  
end
