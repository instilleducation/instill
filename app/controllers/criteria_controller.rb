class CriteriaController < ApplicationController
	before_action :find_as_programme_category, only:[:new,:create,:index,:import,:show,:edit,:update]
	before_action :logged_in_user
	def new
		@criterium = @category.criteria.new
	end

	def create
		@criterium = @category.criteria.new(criterium_params)
		if @criterium.save
			flash[:success] = "Instructional Technique Created"
			redirect_to programme_category_path(@programme,@category)
		else
			render 'new'
		end

	end

	def index
		@criteria = @category.criteria
		respond_to do |format|
      		format.html
      		format.csv{send_data @criteria.to_csv, filename: "#{@category}.csv"}
      	end
    end

  	def import
    	@category.criteria.import(params[:file],@category)
    	redirect_to programme_category_path(@programme,@category)
  	end


	def show
		@criterium = Criterium.find(params[:id])
	end

	def edit
		@criterium = @category.criteria.find(params[:id])


	end

	def update
		@criterium = @category.criteria.find(params[:id])
		if @criterium.update_attributes(criterium_params)
			redirect_to programme_category_criterium_path(@programme,@category, @criterium)
		else
			rennder 'edit'
		end

	end

	def destroy
		@programme = Programme.find(params[:programme_id])
		@category = Category.find(params[:category_id])
		@criterium = Criterium.find(params[:id]).destroy
		flash[:success] = "Instructional Technique Deleted"
		redirect_to programme_category_path(@programme,@category)

	end


	private 

	def find_as_programme_category
		@programme = Programme.find(params[:programme_id])
		@category = @programme.categories.find(params[:category_id])
	end

	def criterium_params
		params.required(:criterium).permit(:name,:focus_points, :possible_score)

	end
end
