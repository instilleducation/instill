class ExitTicketsController < ApplicationController
  before_action :find_as_exit_ticket, only:[:show,:edit,:update,:destroy]
  before_action :logged_in_user  
  def new
    @exit_ticket = ExitTicket.new
  end  

  def create
    @exit_ticket = ExitTicket.new(exit_ticket_params)

    if @exit_ticket.save
      redirect_to @exit_ticket, notice: "Session successfully created."

    end
  end

  def show
    @questions = @exit_ticket.questions

  end

  def index
    @exit_tickets = ExitTicket.all
  end

  def edit
  end

  def update
    if @exit_ticket.update(exit_ticket_params)
      redirect_to @exit_ticket, notice: "Session updated successfully"
    end
  end

  def destroy
    @exit_ticket.destroy
    redirect_to exit_tickets_path
  end

  def add_question
    @exit_ticket = ExitTicket.find(params[:exit_ticket_id])
    @exit_ticket.questions.create(question_text: params[:question_text])
    redirect_to @exit_ticket
  end

  private

  def find_as_exit_ticket
    @exit_ticket = ExitTicket.find(params[:id])
  end

  def exit_ticket_params
    params.require(:exit_ticket).permit(:name)
  end


end
