class StaticPagesController < ApplicationController
  before_action :logged_in_user
  def home
  end

  def dashboard
    @user = User.find(session["user_id"])
    @in_people = InPerson.order("date_done DESC").limit(10)
  end

  def analytics
  	@teachers = Teacher.school(params[:school_ids]) if params[:school_ids].present?
  	@teachers = @teachers.teacher_phase(params[:phases]) if params[:phases].present?
    @teachers = @teachers.observer(params[:observer]) if params[:observer].present?
    @teachers = @teachers.observation_order(params[:observation_orders]) if params[:observation_orders].present?
    @teachers = @teachers.paginate(per_page:10,page:params[:page]) if !@teachers.nil?

    @all_teachers = Teacher.where(nil)
    @all_teachers = @all_teachers.school(params[:school_ids]) if params[:school_ids].present?
    @all_teachers = @all_teachers.teacher_phase(params[:teacher_phase]) if params[:teacher_phase].present?
    @all_teachers = @all_teachers.observer(params[:observer]) if params[:observer].present?
    @all_teachers = @all_teachers.observation_order(params[:observation_order]) if params[:observation_order].present?
    

    @criteria = Criterium.where(nil)
  	@criteria = @criteria.category(params[:category]) if params[:category].present?
  	@criteria = @criteria.criterium(params[:criterium]) if params[:criterium].present?

  end
  
end
