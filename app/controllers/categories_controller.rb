class CategoriesController < ApplicationController
	before_action :find_as_programme, only:[:new,:create,:edit,:update,:index,:show,:destroy]
	before_action :logged_in_user
	def new
		@category = @programme.categories.new
	end

	def create
		@category = @programme.categories.create(category_params)
		if @category.save
			flash[:success] = "Category Created"
			redirect_to programme_category_path(@programme,@category)
		else
			flash[:danger] = "Assignment has not been created"
			render 'new'

		end
	end


	def edit
		@category = Category.find(params[:id])

	end

	def update
		@category = Category.find(params[:id])
		if @category.update_attributes(category_params)
			redirect_to programme_category_path(@programme,@category)
		else
			rennder 'edit'
		end

	end

	def index
		@categories = @programme.categories
	end

	def show
		@category = @programme.categories.find(params[:id])
		@criteria = @category.criteria

		respond_to do |format|
      		format.html
      		format.csv{send_data @criteria.to_csv, filename: "#{@category}.csv"}
    	end
	end


	def destroy
		@programme = Programme.find(params[:programme_id])
		if Category.find(params[:id]).destroy
			flash[:success] = "Category deleted"
			
		else
			flash[:danger] = "Category not Deleted"

		end
		redirect_to @programme
	end


	private 
	def find_as_programme
		@programme = Programme.find(params[:programme_id])
	end

	def category_params
		params.required(:category).permit(:name)

	end

end
