class ProgrammeSessionsController < ApplicationController
    before_action :logged_in_user
    before_action :find_as_programme_session ,only:[:edit,:update,:show,:destroy]
  def new
    @programme_session = ProgrammeSession.new
  end

  def create
    @programme_session = ProgrammeSession.new(programme_session_params)
    @programme = Programme.find(params[:programme_session]["programme_id"])
    type = params[:programme_session]["profile_type"]
    if type == "Observation"
      @programme_session = @programme.programme_sessions.new(profile: Observation.create(),school:@programme_session.school)
    elsif type == "SelfWork"
      @programme_session = @programme.programme_sessions.new(profile: SelfWork.create(),school: @programme_session.school)
    elsif type == "InPerson"
      @programme_session = @programme.programme_sessions.new(profile: InPerson.create(),school: @programme_session.school)
    end
   
    if @programme_session.save
      flash[:success] = "Programme Session Created"
      redirect_to edit_url(@programme_session.profile)

    else
      render 'new'
    end
    
  end

  def show
    @programme = Programme.find(@programme_session.programme_id)
    @profile = @programme_session.profile
    redirect_to profile_url(@profile)
  end





  def edit
    @programme = Programme.find(@programme_session.programme_id)
    @profile = @programme_session.profile
  end

  def update
    @programme = Programme.find(@programme_session.programme_id)
    if @programme_session.update_attributes(programme_session_params)
      redirect_to profile_url(@programme_session.profile), notice: "The session has been updated"
    end
  end

  def index
    @all_programme_sessions = ProgrammeSession.all
    @in_person_sessions = ProgrammeSession.where(profile_type:"InPerson").paginate(page: params[:page]).order("created_at DESC")
    @self_work_sessions = ProgrammeSession.where(profile_type:"SelfWork").paginate(page: params[:page]).order("created_at DESC")
    @observations = ProgrammeSession.where(profile_type:"Observation").paginate(page: params[:page]).order("created_at DESC")
    @programmes = Programme.all
    respond_to do |format|
      format.html
      format.csv {send_data @all_programme_sessions.to_csv, filename: "Programme Sessions = #{Date.today}.csv"}
    end
  end

  def destroy
    if @programme_session.destroy
      flash[:success] = "Session Destroyed"
      redirect_to programme_sessions_path
    else
      flash[:danger] = "Session is not destroyed"
      redirect_to programme_sessions_path
    end
  end

private

  def get_programme_id
    @programme_session.programme_id
  end

  def programme_session_params
    params.require(:programme_session).permit(:programme_id,:school_id,:profile_type)
  end


  def find_as_programme_session
    @programme_session = ProgrammeSession.find(params[:id])
  end


  def context
    if params[:programme_session][:profile_type] == "Self Work"
      @self_work = SelfWork.find(@programme_session.profile_id)
    elsif params[:programme_session][:profile_type] == "Observation"
      @observation = Observation.find(@programme_session.profile_id)
    elsif params[:programme_session][:profile_type] == "In Person Training"
      @inPersonTraining = InPersonTraining.find(@programme_session.profile_id)
    end
  end




  def profile_url(profile)
    if profile.class == SelfWork
      self_work_path(profile)
    elsif profile.class == Observation 
      observation_path(profile)
    elsif profile.class == InPerson
      in_person_path(profile)      
    end
  end

  def new_url(profile)
    if profile.class == SelfWork
      new_self_work_path(profile)
    elsif profile.class == Observation 
      new_observation_path(profile)
    elsif profile.class == InPerson
      new_in_person_path(profile)      
    end
  end

    def edit_url(profile)
    if profile.class == SelfWork
      edit_self_work_path(profile)
    elsif profile.class == Observation 
      edit_observation_path(profile)
    elsif profile.class == InPerson
      edit_in_person_path(profile)      
    end
  end


end
