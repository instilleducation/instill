class QuestionsController < ApplicationController
  before_action :find_as_exit_ticket, only:[:edit,:update,:destroy]
  before_action :logged_in_user
  
  def new
  	@question = @exit_ticket.questions.new
  end

  def create

  	@question = @exit_ticket.questions.build(question_params)
  		if @question.save
  			redirect_to @exit_ticket	
      else
        redirect_to @exit_ticket
        flash[:danger] = "Question is not created"
      end
  end

  def index
    @questions = @exit_ticket.questions
  end


  def edit
  	@question = @exit_ticket.questions.find(params[:id])
  end

  def update
  	@question = @exit_ticket.questions.find(params[:id])
  	if @question.update_attributes(question_params)
      redirect_to @exit_ticket
    else
      redirect_to @exit_ticket
      flash[:danger] = "Can't be updated"
    end 	

  end

  
  def destroy
    @question = @exit_ticket.questions.find(params[:id])
    if @question.destroy
      redirect_to @exit_ticket
    else
      flash[:danger] = "Can't be deleted"
    end
  end



  private

  def find_as_exit_ticket
    @exit_ticket = ExitTicket.find(params[:exit_ticket_id])
  end

  def question_params
    params.require(:question).permit(:question_text)
  end


end
