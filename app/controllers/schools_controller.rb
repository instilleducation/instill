class SchoolsController < ApplicationController
    before_action :logged_in_user
     before_action :find_as_school, only: [:edit,:show,:update,:destroy,:create_conversation]
  def new
  	@school = School.new
  end

  def show
    @teachers = @school.teachers.paginate(page: params[:page], per_page: 20)
    @conversations = @school.conversations
    @scores = @scores.school(params[:school]) if params[:school].present?
      respond_to do |format|
      format.html
      format.csv {send_data @teachers.to_contacts_csv, filename: "#{@school.name} Teachers - #{Date.today}.csv"}
    end
  end

  def index
  	@schools = School.all
  end

  def create
  	@school = School.new(school_params)
  	if @school.save
  		flash[:success] = "School Created"
  		redirect_to @school
  	end
  end

  def import 
    School.import(params[:file])
    redirect_to schools_path
  end

  

  def edit
  end

  def update
  	if @school.update_attributes(school_params)
  		redirect_to @school
  	else
  		render 'edit'
  	end
  end



  def destroy
  	if @school.destroy
      flash[:success] = "School Deleted"
  	  redirect_to schools_path
    else
      flash[:danger] = "School not deleted"
      redirect_to @school
    end
  end


  def create_conversation
    @school = School.find(params[:id])
    @programme = Programme.find(params[:programme_id])

    @conversation = Conversation.create(school:@school,programme:@programme,
      month:params[:month],
      status:params[:status])
  end

  def create_sheet_email
    @school = School.find(params[:format])
    @teachers = @school.teachers.prefer_email
    user = User.find(session["user_id"])
    if user.token.nil?
      redirect_to '/auth/google_oauth2'
    else
      @teachers.create_spreadsheet(user,"Teachers Email #{@school.name}","contact")
      redirect_to @school
    end
  end

  def create_sheet_sms
    @school = School.find(params[:format])
    @teachers = @school.teachers.prefer_sms
    user = User.find(session["user_id"])
    if user.token.nil?
      redirect_to '/auth/google_oauth2'
    else
      @teachers.create_spreadsheet(user,"Teacher SMS #{@school.name}","contact")

      redirect_to @school
    end
  end

  

  private

  def find_as_school
    @school = School.find(params[:id])
  end
  
  def school_params
  	params.require(:school).permit(:name, :address, programme_ids:[],
     conversation_attritubes:[:id,:month,:status])
  end

end
