module ProgrammeSessionsHelper
	def view_type(profile_type)
	    if profile_type == "SelfWork" 
	      return "Self Work"
	    elsif profile_type == "Observation" 
	      return "Observation"
	    elsif profile_type == "InPerson" 
	      return "In Person Training"     
	    end
  	end
end
