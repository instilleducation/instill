module ApplicationHelper

	def observation_growth(teacher)
		first_observation = first_observation(teacher)
        last_observation = last_observation(teacher)

        return (last_observation - first_observation).round(2)
    end

    def first_observation(teacher)
        observations = (1..teacher.school.programme_sessions.where(profile_type:"Observation").count)
        first_observation = 0
        observations.each do |observation|
            if !teacher.scores.observation_order(observation).average(:score_value).nil?
                first_observation = teacher.scores.observation_order(observation).average(:score_value).round(2)
            end
        end
        return first_observation
    end

    def last_observation(teacher)
        observations = (1..teacher.school.programme_sessions.where(profile_type:"Observation").count)
        last_observation = 0
        observations.reverse_each do |observation|
            if !teacher.scores.observation_order(observation).average(:score_value).nil?
                last_observation = teacher.scores.observation_order(observation).average(:score_value).round(2)
            end
        end
        return last_observation
    end



end
