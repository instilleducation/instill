# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)



User.create(name:"Maryke Bailey", email: "maryke@instill.education", admin: false, 
	password:"!nstillis@wesome", password_confirmation: "!nstillis@wesome")

User.create(name:"Heather Kayton", email: "heather@instill.education", admin: false, 
	password:"!nstillis@wesome", password_confirmation: "!nstillis@wesome")

User.create(name:"Jayd Georgiades", email: "jayd@instill.education", admin: false, 
	password:"!nstillis@wesome", password_confirmation: "!nstillis@wesome")

User.create(name:"Margaret McGillivray", email: "margaret@instill.education", admin: false, 
	password:"!nstillis@wesome", password_confirmation: "!nstillis@wesome")

User.create(name:"Ntando Mlilo", email: "ntando@instill.education", admin: false, 
	password:"!nstillis@wesome", password_confirmation: "!nstillis@wesome")

User.create(name:"Dawn Mbalekwa", email: "dawn@instill.education", admin: false, 
	password:"!nstillis@wesome", password_confirmation: "!nstillis@wesome")

