# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180530172923) do

  create_table "action_steps", force: :cascade do |t|
    t.string "as_name"
    t.integer "as_code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "as_categories", force: :cascade do |t|
    t.string "name"
    t.integer "action_step_id"
    t.integer "takeaway_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["action_step_id"], name: "index_as_categories_on_action_step_id"
    t.index ["takeaway_id"], name: "index_as_categories_on_takeaway_id"
  end

  create_table "assignment_grades", force: :cascade do |t|
    t.integer "assignment_id"
    t.integer "submission_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "grade_given"
    t.index ["assignment_id"], name: "index_assignment_grades_on_assignment_id"
    t.index ["submission_id"], name: "index_assignment_grades_on_submission_id"
  end

  create_table "assignments", force: :cascade do |t|
    t.string "name"
    t.integer "self_work_id"
    t.integer "possible_score"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "description"
    t.index ["self_work_id"], name: "index_assignments_on_self_work_id"
  end

  create_table "attendances", force: :cascade do |t|
    t.integer "in_person_id"
    t.integer "teacher_id"
    t.integer "school_id"
    t.string "attendance_value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["in_person_id"], name: "index_attendances_on_in_person_id"
    t.index ["school_id"], name: "index_attendances_on_school_id"
    t.index ["teacher_id"], name: "index_attendances_on_teacher_id"
  end

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "programme_id"
    t.index ["programme_id"], name: "index_categories_on_programme_id"
  end

  create_table "conversations", force: :cascade do |t|
    t.integer "programme_id"
    t.integer "school_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.date "month"
    t.string "status"
    t.index ["programme_id"], name: "index_conversations_on_programme_id"
    t.index ["school_id"], name: "index_conversations_on_school_id"
  end

  create_table "criteria", force: :cascade do |t|
    t.string "name"
    t.text "focus_points"
    t.integer "possible_score"
    t.integer "category_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["category_id"], name: "index_criteria_on_category_id"
  end

  create_table "developments", force: :cascade do |t|
    t.integer "school_id"
    t.integer "programme_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["programme_id"], name: "index_developments_on_programme_id"
    t.index ["school_id"], name: "index_developments_on_school_id"
  end

  create_table "exit_ticket_answers", force: :cascade do |t|
    t.integer "question_id"
    t.float "percentage_correct"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "in_person_id"
    t.index ["in_person_id"], name: "index_exit_ticket_answers_on_in_person_id"
    t.index ["question_id"], name: "index_exit_ticket_answers_on_question_id"
  end

  create_table "exit_tickets", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "feedbacks", force: :cascade do |t|
    t.datetime "observation_done"
    t.integer "teacher_id"
    t.integer "user_id"
    t.text "pp_effective_1"
    t.text "pp_effective_2"
    t.text "pp_impact_1"
    t.text "pp_impact_2"
    t.integer "observation_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "first_ontask"
    t.integer "second_ontask"
    t.integer "first_offtask"
    t.integer "second_offtask"
    t.index ["observation_id"], name: "index_feedbacks_on_observation_id"
    t.index ["teacher_id"], name: "index_feedbacks_on_teacher_id"
    t.index ["user_id"], name: "index_feedbacks_on_user_id"
  end

  create_table "in_people", force: :cascade do |t|
    t.string "name"
    t.date "date_done"
    t.string "in_person_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.date "scheduled_date"
    t.integer "exit_ticket_id"
    t.integer "expected_attendance"
    t.float "scheduled_duration"
    t.float "actual_duration"
    t.index ["exit_ticket_id"], name: "index_in_people_on_exit_ticket_id"
  end

  create_table "levers", force: :cascade do |t|
    t.text "highest_lever"
    t.integer "category_id"
    t.integer "feedback_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["category_id"], name: "index_levers_on_category_id"
    t.index ["feedback_id"], name: "index_levers_on_feedback_id"
  end

  create_table "observations", force: :cascade do |t|
    t.string "name"
    t.string "observation_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "observation_order"
  end

  create_table "programme_sessions", force: :cascade do |t|
    t.integer "programme_id"
    t.integer "profile_id"
    t.string "profile_type"
    t.integer "school_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["programme_id"], name: "index_programme_sessions_on_programme_id"
    t.index ["school_id"], name: "index_programme_sessions_on_school_id"
  end

  create_table "programmes", force: :cascade do |t|
    t.string "name"
    t.date "start_date"
    t.date "end_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "questions", force: :cascade do |t|
    t.text "question_text"
    t.integer "exit_ticket_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["exit_ticket_id"], name: "index_questions_on_exit_ticket_id"
  end

  create_table "schools", force: :cascade do |t|
    t.string "name"
    t.string "address"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "scores", force: :cascade do |t|
    t.integer "feedback_id"
    t.integer "criterium_id"
    t.integer "score_value"
    t.text "score_notes"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["criterium_id"], name: "index_scores_on_criterium_id"
    t.index ["feedback_id"], name: "index_scores_on_feedback_id"
  end

  create_table "sections", force: :cascade do |t|
    t.integer "category_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "programme_id"
    t.index ["category_id"], name: "index_sections_on_category_id"
    t.index ["programme_id"], name: "index_sections_on_programme_id"
  end

  create_table "self_works", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "submissions", force: :cascade do |t|
    t.integer "self_work_id"
    t.float "percentage_complete"
    t.integer "time_taken"
    t.integer "teacher_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "school_id"
    t.index ["school_id"], name: "index_submissions_on_school_id"
    t.index ["self_work_id"], name: "index_submissions_on_self_work_id"
    t.index ["teacher_id"], name: "index_submissions_on_teacher_id"
  end

  create_table "takeaways", force: :cascade do |t|
    t.integer "feedback_id"
    t.text "what_happened"
    t.text "prep"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["feedback_id"], name: "index_takeaways_on_feedback_id"
  end

  create_table "teachers", force: :cascade do |t|
    t.string "full_name"
    t.string "email"
    t.integer "school_id"
    t.string "status"
    t.string "phone"
    t.string "sace_no"
    t.string "grade"
    t.string "subject"
    t.string "id_number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "qualification"
    t.string "university"
    t.string "role"
    t.string "gender"
    t.integer "years_of_teaching"
    t.integer "age"
    t.string "communication"
    t.index ["school_id"], name: "index_teachers_on_school_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.boolean "admin"
    t.string "password_digest"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "token"
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "refresh_token"
    t.string "expires_at"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
