class CreateQuestions < ActiveRecord::Migration[5.1]
  def change
    create_table :questions do |t|
      t.text :question_text
      t.references :exit_ticket, foreign_key: true
      t.float :percentage_correct

      t.timestamps
    end
  end
end
