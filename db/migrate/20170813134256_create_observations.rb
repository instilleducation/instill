class CreateObservations < ActiveRecord::Migration[5.1]
  def change
    create_table :observations do |t|
      t.string :name
      t.string :observation_type

      t.timestamps
    end
  end
end
