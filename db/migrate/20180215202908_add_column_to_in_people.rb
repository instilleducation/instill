class AddColumnToInPeople < ActiveRecord::Migration[5.1]
  def change
    add_reference :in_people, :exit_ticket, foreign_key: true
  end
end
