class CreateAsCategories < ActiveRecord::Migration[5.1]
  def change
    create_table :as_categories do |t|
      t.string :name
      t.references :action_step, foreign_key: true
      t.references :takeaway, foreign_key: true

      t.timestamps
    end
  end
end
