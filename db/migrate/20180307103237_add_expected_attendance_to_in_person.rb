class AddExpectedAttendanceToInPerson < ActiveRecord::Migration[5.1]
  def change
    add_column :in_people, :expected_attendance, :integer
  end
end
