class AddSchoolsToSubmissions < ActiveRecord::Migration[5.1]
  def change
    add_reference :submissions, :school, foreign_key: true
  end
end
