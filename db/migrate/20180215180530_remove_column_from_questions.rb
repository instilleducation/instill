class RemoveColumnFromQuestions < ActiveRecord::Migration[5.1]
  def change
    remove_column :questions, :percentage_correct, :string
  end
end
