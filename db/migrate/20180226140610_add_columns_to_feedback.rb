class AddColumnsToFeedback < ActiveRecord::Migration[5.1]
  def change
    add_column :feedbacks, :first_ontask, :integer
    add_column :feedbacks, :second_ontask, :integer
    add_column :feedbacks, :first_offtask, :integer
    add_column :feedbacks, :second_offtask, :integer
  end
end
