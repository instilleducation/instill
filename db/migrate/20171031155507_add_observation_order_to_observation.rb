class AddObservationOrderToObservation < ActiveRecord::Migration[5.1]
  def change
    add_column :observations, :observation_order, :integer
  end
end
