class CreateExitTickets < ActiveRecord::Migration[5.1]
  def change
    create_table :exit_tickets do |t|
      t.string :name

      t.timestamps
    end
  end
end
