class ChangeHightestLeverToHighestLever < ActiveRecord::Migration[5.1]
  def change
  	rename_column :levers, :hightest_lever, :highest_lever
  end
end
