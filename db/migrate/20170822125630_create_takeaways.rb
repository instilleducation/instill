class CreateTakeaways < ActiveRecord::Migration[5.1]
  def change
    create_table :takeaways do |t|
      t.references :feedback, foreign_key: true
      t.references :action_step, foreign_key: true
      t.text :what_happened
      t.text :prep

      t.timestamps
    end
  end
end
