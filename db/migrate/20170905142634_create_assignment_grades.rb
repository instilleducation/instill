class CreateAssignmentGrades < ActiveRecord::Migration[5.1]
  def change
    create_table :assignment_grades do |t|
      t.references :assignment, foreign_key: true
      t.references :submission, foreign_key: true

      t.timestamps
    end
  end
end
