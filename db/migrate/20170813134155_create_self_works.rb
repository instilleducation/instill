class CreateSelfWorks < ActiveRecord::Migration[5.1]
  def change
    create_table :self_works do |t|
      t.string :name

      t.timestamps
    end
  end
end
