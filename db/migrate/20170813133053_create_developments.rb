class CreateDevelopments < ActiveRecord::Migration[5.1]
  def change
    create_table :developments do |t|
      t.references :school, foreign_key: true
      t.references :programme, foreign_key: true

      t.timestamps
    end
  end
end
