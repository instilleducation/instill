class CreateProgrammeSessions < ActiveRecord::Migration[5.1]
  def change
    create_table :programme_sessions do |t|
      t.references :programme, foreign_key: true
      t.integer :profile_id
      t.string :profile_type
      t.references :school, foreign_key: true

      t.timestamps
    end
  end
end
