class AddColumnToInPerson < ActiveRecord::Migration[5.1]
  def change
    add_column :in_people, :scheduled_date, :date
  end
end
