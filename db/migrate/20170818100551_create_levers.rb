class CreateLevers < ActiveRecord::Migration[5.1]
  def change
    create_table :levers do |t|
      t.text :hightest_lever
      t.references :category, foreign_key: true
      t.references :teacher, foreign_key: true
      t.references :feedback, foreign_key: true

      t.timestamps
    end
  end
end
