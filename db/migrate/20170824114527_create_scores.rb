class CreateScores < ActiveRecord::Migration[5.1]
  def change
    create_table :scores do |t|
      t.references :feedback, foreign_key: true
      t.references :criterium, foreign_key: true
      t.integer :score_value
      t.text :score_notes

      t.timestamps
    end
  end
end
