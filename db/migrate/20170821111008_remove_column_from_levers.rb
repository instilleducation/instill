class RemoveColumnFromLevers < ActiveRecord::Migration[5.1]
  def change
    remove_column :levers, :teacher_id, :integer
  end
end
