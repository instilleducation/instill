class RemoveColumnFromSections < ActiveRecord::Migration[5.1]
  def change
  	remove_column :sections, :observation_id, :integer
  end
end
