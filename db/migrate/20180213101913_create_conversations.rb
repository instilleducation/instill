class CreateConversations < ActiveRecord::Migration[5.1]
  def change
    create_table :conversations do |t|
      t.references :programme, foreign_key: true
      t.references :school, foreign_key: true
      t.integer :number

      t.timestamps
    end
  end
end
