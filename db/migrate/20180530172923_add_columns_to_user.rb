class AddColumnsToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :refresh_token, :string
    add_column :users, :expires_at, :string
  end
end
