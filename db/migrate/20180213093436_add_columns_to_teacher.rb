class AddColumnsToTeacher < ActiveRecord::Migration[5.1]
  def change
    add_column :teachers, :qualification, :string
    add_column :teachers, :university, :string
    add_column :teachers, :role, :string
    add_column :teachers, :gender, :string
    add_column :teachers, :years_of_teaching, :integer
    add_column :teachers, :age, :integer
    add_column :teachers, :communication, :string
  end
end
