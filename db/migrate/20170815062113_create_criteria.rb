class CreateCriteria < ActiveRecord::Migration[5.1]
  def change
    create_table :criteria do |t|
      t.string :name
      t.text :focus_points
      t.integer :possible_score
      t.references :category, foreign_key: true

      t.timestamps
    end
  end
end
