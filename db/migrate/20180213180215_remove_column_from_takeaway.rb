class RemoveColumnFromTakeaway < ActiveRecord::Migration[5.1]
  def change
    remove_reference :takeaways, :action_step, foreign_key: true
  end
end
