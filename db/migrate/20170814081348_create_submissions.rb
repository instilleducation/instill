class CreateSubmissions < ActiveRecord::Migration[5.1]
  def change
    create_table :submissions do |t|
      t.references :self_work, foreign_key: true
      t.float :percentage_complete
      t.integer :time_taken
      t.references :teacher, foreign_key: true

      t.timestamps
    end
  end
end
