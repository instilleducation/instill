class CreateAssignments < ActiveRecord::Migration[5.1]
  def change
    create_table :assignments do |t|
      t.string :name
      t.references :self_work, foreign_key: true
      t.integer :possible_score

      t.timestamps
    end
  end
end
