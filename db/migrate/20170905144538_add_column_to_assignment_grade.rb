class AddColumnToAssignmentGrade < ActiveRecord::Migration[5.1]
  def change
    add_column :assignment_grades, :grade_given, :integer
  end
end
