class CreateActionSteps < ActiveRecord::Migration[5.1]
  def change
    create_table :action_steps do |t|
      t.string :as_category
      t.string :as_name
      t.integer :as_code

      t.timestamps
    end
  end
end
