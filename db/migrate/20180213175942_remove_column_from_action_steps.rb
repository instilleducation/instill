class RemoveColumnFromActionSteps < ActiveRecord::Migration[5.1]
  def change
    remove_column :action_steps, :as_category, :string
  end
end
