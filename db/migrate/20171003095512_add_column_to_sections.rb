class AddColumnToSections < ActiveRecord::Migration[5.1]
  def change
    add_reference :sections, :programme, foreign_key: true
  end
end
