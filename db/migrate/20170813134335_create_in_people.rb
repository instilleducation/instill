class CreateInPeople < ActiveRecord::Migration[5.1]
  def change
    create_table :in_people do |t|
      t.string :name
      t.date :date_done
      t.string :in_person_type

      t.timestamps
    end
  end
end
