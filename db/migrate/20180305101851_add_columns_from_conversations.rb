class AddColumnsFromConversations < ActiveRecord::Migration[5.1]
  def change
    add_column :conversations, :month, :date
    add_column :conversations, :status, :string
  end
end
