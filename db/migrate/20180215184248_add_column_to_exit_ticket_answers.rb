class AddColumnToExitTicketAnswers < ActiveRecord::Migration[5.1]
  def change
    add_reference :exit_ticket_answers, :in_person, foreign_key: true
  end
end
