class CreateTeachers < ActiveRecord::Migration[5.1]
  def change
    create_table :teachers do |t|
      t.string :full_name
      t.string :email
      t.references :school, foreign_key: true
      t.string :status
      t.string :phone
      t.string :sace_no
      t.string :grade
      t.string :subject
      t.string :id_number

      t.timestamps
    end
  end
end
