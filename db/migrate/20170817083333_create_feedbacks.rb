class CreateFeedbacks < ActiveRecord::Migration[5.1]
  def change
    create_table :feedbacks do |t|
      t.datetime :observation_done
      t.references :teacher, foreign_key: true
      t.references :user, foreign_key: true
      t.text :pp_effective_1
      t.text :pp_effective_2
      t.text :pp_impact_1
      t.text :pp_impact_2
      t.references :observation, foreign_key: true

      t.timestamps
    end
  end
end
