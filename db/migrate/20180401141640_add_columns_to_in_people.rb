class AddColumnsToInPeople < ActiveRecord::Migration[5.1]
  def change
    add_column :in_people, :scheduled_duration, :float
    add_column :in_people, :actual_duration, :float
  end
end
