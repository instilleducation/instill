class CreateAttendances < ActiveRecord::Migration[5.1]
  def change
    create_table :attendances do |t|
      t.references :in_person, foreign_key: true
      t.references :teacher, foreign_key: true
      t.references :school, foreign_key: true
      t.string :attendance_value

      t.timestamps
    end
  end
end
