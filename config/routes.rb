Rails.application.routes.draw do
  


  match "/auth/google_oauth2/callback" , to: 'google_auth#create', via: 'get'
  get "auth/failure", to: redirect('/')

  get 'scores/index'

  get 'sessions/new'
  root 'static_pages#dashboard'
  get 'static_pages/home'
  
  get 'static_pages/dashboard'
  get 'static_pages/analytics'



  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  delete 'logout', to: 'sessions#destroy'
  


  resources :users do 
    collection do
      get :authenticate
    end
  end
  
  resources :teachers do 
    collection do 
      post :import
      get :to_google_sheet
      get :create_sheet
    end
  end
  resources :developments

  resources :schools do  
    collection do
      post :import 
      get :create_sheet_email
      get :create_sheet_sms
    end
    resources :conversations do
        put :increase_number
    end
  end

  resources :as_categories 
  
  resources :action_steps do 
    collection do
        post :import
    end
  end

  resources :programmes do
    get :update_spreadsheet
    resources :categories do 
      resources :criteria do 
        collection do 
          post :import
        end
      end
    
    end  
  end

  resources :in_people do
    get 'download', on: :member, constraints: {format: /(html|csv)/}
    post :add_exit_ticket
    post :add_question_answers
    post :create_attendance
    post :create_mulitple_attendances
    resources :attendances do
      collection do
        post :import
        get :edit_multiple
        put :update_multiple
      end
    end
    resources :exit_ticket_answers do
      collection do
        get :edit_all
        put :update_all
      end
    end
  end

  resources :exit_tickets do
      post :add_question
      resources :questions
  end

  resources :self_works do
    resources :submissions do
      collection do
        post :import
      end
    end

    resources :assignments
  end
  resources :observations do 
    resources :feedbacks do
      post :add_score
      post :add_lever
      collection do 
        post :import
        post :import_google
      end
       
    end
  end
  
  resources :programme_sessions do
    resources :in_people
    resources :self_works
    resources :observations
  end

  resources :scores, only: [:index]
    # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
