require 'test_helper'

class ActionStepsControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get action_steps_new_url
    assert_response :success
  end

  test "should get edit" do
    get action_steps_edit_url
    assert_response :success
  end

  test "should get index" do
    get action_steps_index_url
    assert_response :success
  end

  test "should get show" do
    get action_steps_show_url
    assert_response :success
  end

end
