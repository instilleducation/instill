require 'test_helper'

class AttendancesControllerTest < ActionDispatch::IntegrationTest
  test "should get edit" do
    get attendances_edit_url
    assert_response :success
  end

  test "should get new" do
    get attendances_new_url
    assert_response :success
  end

end
