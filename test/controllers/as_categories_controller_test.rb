require 'test_helper'

class AsCategoriesControllerTest < ActionDispatch::IntegrationTest
  test "should get show" do
    get as_categories_show_url
    assert_response :success
  end

  test "should get index" do
    get as_categories_index_url
    assert_response :success
  end

  test "should get edit" do
    get as_categories_edit_url
    assert_response :success
  end

  test "should get destroy" do
    get as_categories_destroy_url
    assert_response :success
  end

end
