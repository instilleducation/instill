require 'test_helper'

class SelfWorksControllerTest < ActionDispatch::IntegrationTest
  test "should get edit" do
    get self_works_edit_url
    assert_response :success
  end

  test "should get index" do
    get self_works_index_url
    assert_response :success
  end

  test "should get new" do
    get self_works_new_url
    assert_response :success
  end

  test "should get show" do
    get self_works_show_url
    assert_response :success
  end

end
