require 'test_helper'

class ProgrammeSessionsControllerTest < ActionDispatch::IntegrationTest
  test "should get edit" do
    get programme_sessions_edit_url
    assert_response :success
  end

  test "should get index" do
    get programme_sessions_index_url
    assert_response :success
  end

  test "should get new" do
    get programme_sessions_new_url
    assert_response :success
  end

  test "should get show" do
    get programme_sessions_show_url
    assert_response :success
  end

end
