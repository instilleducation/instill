require 'test_helper'

class InPeopleControllerTest < ActionDispatch::IntegrationTest
  test "should get edit" do
    get in_people_edit_url
    assert_response :success
  end

  test "should get index" do
    get in_people_index_url
    assert_response :success
  end

  test "should get new" do
    get in_people_new_url
    assert_response :success
  end

  test "should get show" do
    get in_people_show_url
    assert_response :success
  end

end
