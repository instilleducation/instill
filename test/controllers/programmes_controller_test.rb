require 'test_helper'

class ProgrammesControllerTest < ActionDispatch::IntegrationTest
  test "should get edit" do
    get programmes_edit_url
    assert_response :success
  end

  test "should get index" do
    get programmes_index_url
    assert_response :success
  end

  test "should get new" do
    get programmes_new_url
    assert_response :success
  end

  test "should get show" do
    get programmes_show_url
    assert_response :success
  end

end
