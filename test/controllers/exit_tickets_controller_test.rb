require 'test_helper'

class ExitTicketsControllerTest < ActionDispatch::IntegrationTest
  test "should get show" do
    get exit_tickets_show_url
    assert_response :success
  end

  test "should get index" do
    get exit_tickets_index_url
    assert_response :success
  end

  test "should get edit" do
    get exit_tickets_edit_url
    assert_response :success
  end

  test "should get destroy" do
    get exit_tickets_destroy_url
    assert_response :success
  end

  test "should get new" do
    get exit_tickets_new_url
    assert_response :success
  end

end
